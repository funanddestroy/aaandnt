﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kronecker__multivariate_
{
    class Polynomial
    {
        private int[] coefficients;

        public Polynomial(params int[] coefficients)
        {
            this.coefficients = coefficients;
        }

        public Polynomial(Polynomial o)
        {
            this.coefficients = (int[])o.coefficients.Clone();
        }

        public int this[int n]
        {
            get { return coefficients[n]; }
            set { coefficients[n] = value; }
        }

        public int deg()
        {
            int i = coefficients.Length - 1;

            while (i > 0 && coefficients[i] == 0)
            {
                i--;
            }

            return i;
        }

        public int сalculate(int x)
        {
            int n = coefficients.Length - 1;
            int result = 0;
            for (int i = n; i >= 0; i--)
            {
                result = x * result + coefficients[i];
            }
            return result;
        }

        public int[] getArray()
        {
            int[] ret = new int[coefficients.Length];
            Array.Copy(coefficients, ret, coefficients.Length);
            return ret;
        }

        public void trim()
        {
            int ord = this.deg();
            int[] res = new int[ord + 1];
            for (int i = 0; i < ord + 1; i++)
            {
                res[i] = this.coefficients[i];
            }

            this.coefficients = res;
        }

        public override string ToString()
        {
            int ord = this.deg();
            string ret = "";
            for (int i = ord; i > 1; i--)
            {
                if (coefficients[i] != 0)
                {
                    ret += coefficients[i] + "x^" + i;
                    if (i - 1 != 0 && i > 3)
                    {
                        ret += " + ";
                    }
                }
            }

            if (ord > 0 && coefficients[1] != 0 && ret.Equals(""))
            {
                ret += coefficients[1] + "x";
            }
            else if (ord > 0 && coefficients[1] != 0)
            {
                ret += " + " + coefficients[1] + "x";
            }

            if (ord >= 0 && coefficients[0] != 0 && ret.Equals(""))
            {
                ret += coefficients[0];
            }
            else if (ord >= 0 && coefficients[0] != 0)
            {
                ret += " + " + coefficients[0];
            }

            return ret;
        }

        public bool isZero()
        {
            for (int i = 0; i < coefficients.Length; i++)
            {
                if (coefficients[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static Polynomial operator +(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a + b;
            }
            return new Polynomial(result);
        }

        public static Polynomial operator -(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a - b;
            }
            return new Polynomial(result);
        }

        public static Polynomial operator *(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = pFirst.coefficients.Length + pSecond.coefficients.Length - 1;
            var result = new int[itemsCount];
            for (int i = 0; i < pFirst.coefficients.Length; i++)
            {
                for (int j = 0; j < pSecond.coefficients.Length; j++)
                {
                    result[i + j] += pFirst[i] * pSecond[j];
                }
            }

            return new Polynomial(result);
        }

        public static Polynomial operator /(Polynomial pFirst, Polynomial pSecond)
        {
            pFirst.trim();
            pSecond.trim();

            int[] dividend = pFirst.getArray();
            int[] divisor = pSecond.getArray();

            if (dividend.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делимого не может быть 0");
            }
            if (divisor.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делителя не может быть 0");
            }

            int[] remainder = (int[])dividend.Clone();
            int[] quotient = new int[remainder.Length - divisor.Length + 1];
            for (int i = 0; i < quotient.Length; i++)
            {
                int coeff = remainder[remainder.Length - i - 1] / divisor.Last();
                quotient[quotient.Length - i - 1] = coeff;
                for (int j = 0; j < divisor.Length; j++)
                {
                    remainder[remainder.Length - i - j - 1] -= coeff * divisor[divisor.Length - j - 1];
                }
            }

            return new Polynomial(quotient);
        }

        public static Polynomial operator %(Polynomial pFirst, Polynomial pSecond)
        {
            pFirst.trim();
            pSecond.trim();

            int[] dividend = pFirst.getArray();
            int[] divisor = pSecond.getArray();

            if (dividend.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делимого не может быть 0");
            }
            if (divisor.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делителя не может быть 0");
            }

            int[] remainder = (int[])dividend.Clone();
            int[] quotient = new int[remainder.Length - divisor.Length + 1];
            for (int i = 0; i < quotient.Length; i++)
            {
                int coeff = remainder[remainder.Length - i - 1] / divisor.Last();
                quotient[quotient.Length - i - 1] = coeff;
                for (int j = 0; j < divisor.Length; j++)
                {
                    remainder[remainder.Length - i - j - 1] -= coeff * divisor[divisor.Length - j - 1];
                }
            }

            return new Polynomial(remainder);
        }
    }
}
