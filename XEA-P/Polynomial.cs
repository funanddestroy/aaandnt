﻿using System;

namespace XEA_P
{
    class Polynomial
    {
        private readonly int[] coefficients;
        private static int m;

        public Polynomial(int m1, params int[] coefficients)
        {
            m = m1;
            this.coefficients = coefficients;
        }

        public Polynomial(int m1, Polynomial o)
        {
            m = m1;
            this.coefficients = (int[])o.coefficients.Clone();
        }
        
        public int this[int n]
        {
            get { return coefficients[n]; }
            set { coefficients[n] = value; }
        }
        
        public int Order()
        {
            int i = coefficients.Length - 1;
            while (coefficients[i] == 0)
            {
                i--;
            }

            return i;
        }

        public override string ToString()
        {
            int ord = this.Order();
            string ret = "";
            for (int i = ord; i > 1; i--)
            {
                if (coefficients[i] != 0)
                {
                    ret += coefficients[i] + "x^" + i;
                    if (i - 1 != 0 && i > 3)
                    {
                        ret += " + ";
                    }
                }
            }

            if (ord > 0 && coefficients[1] != 0 && ret.Equals(""))
            {
                ret += coefficients[1] + "x";
            }
            else if (ord > 0 && coefficients[1] != 0)
            {
                ret += " + " + coefficients[1] + "x";
            }

            if (ord >= 0 && coefficients[0] != 0 && ret.Equals(""))
            {
                ret += coefficients[0];
            }
            else if (ord >= 0 && coefficients[0] != 0)
            {
                ret += " + " + coefficients[0];
            }

            return ret;
        }

        public bool isZero()
        {
            for (int i = 0; i < coefficients.Length; i++)
            {
                if (coefficients[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static Polynomial operator +(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = ((a + b) % m + m) % m;
            }
            return new Polynomial(m, result);
        }

        public static Polynomial operator -(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = ((a - b) % m + m) % m;
            }
            return new Polynomial(m, result);
        }

        public static Polynomial operator *(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = pFirst.coefficients.Length + pSecond.coefficients.Length - 1;
            var result = new int[itemsCount];
            for (int i = 0; i < pFirst.coefficients.Length; i++)
            {
                for (int j = 0; j < pSecond.coefficients.Length; j++)
                {
                    result[i + j] = (result[i + j] + pFirst[i] * pSecond[j]) % m;
                }
            }

            return new Polynomial(m, result);
        }
    }
}
