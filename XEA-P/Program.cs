﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace XEA_P
{
    class Program
    {
        private static int gcdex(int a, int b, int x, int y, out int outX, out int outY)
        {
            if (a == 0)
            {
                outX = 0; outY = 1;
                return b;
            }
            int x1 = 0;
            int y1 = 0;
            int d = gcdex(b % a, a, x1, y1, out x1, out y1);
            outX = y1 - (b / a) * x1;
            outY = x1;
            return d;
        }

        private static int inverse(int a, int m)
        {
            a = (a % m + m) % m;
            int x = 0;
            int y = 0;
            int g = gcdex(a, m, x, y, out x, out y);
            if (g != 1)
                return -1;
            else
            {
                x = (x % m + m) % m;
                return x;
            }
        }

        private static Polynomial pdf(Polynomial p1, Polynomial p2, int p, out Polynomial r)
        {
            int m = p1.Order();
            int n = p2.Order();

            if (m < n)
            {
                Polynomial tmp = p1;
                p1 = p2;
                p2 = tmp;
                int tp = n;
                n = m;
                m = tp;
            }

            Polynomial q = new Polynomial(p, new int[m - n + 1]);

            for (int k = m - n; k >= 0; k--)
            {
                q[k] = ((p1[n + k] * inverse(p2[n], p)) % p + p) % p;
                for (int j = n + k - 1; j >= k; j--)
                {
                    p1[j] = ((p1[j] - q[k] * p2[j - k]) % p + p) % p;
                }
            }

            r = new Polynomial(p, new int[] { 0 });

            if (n > 0)
            {
                r = new Polynomial(p, new int[n]);
                for (int i = 0; i < n; i++)
                {
                    r[i] = p1[i];
                }
            }

            return q;
        }

        static void Main(string[] args)
        {
            Console.WriteLine("╔═════════════════════════════════════════════════════════════════════════════╗");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║                                     XEA-P                                   ║");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║           Программа для расчета НОД многочленов с использованием            ║");
            Console.WriteLine("║        расширенного алгоритма Евклида над конечным полем целых чисел        ║");
            Console.WriteLine("║            Кроме того расчитываются полиномы g и f из соотношения           ║");
            Console.WriteLine("║                           p1*g + p2*f = НОД(p1, p2)                         ║");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("╚═════════════════════════════════════════════════════════════════════════════╝");
            Console.WriteLine();



            Console.Write("Введите простое число - мощность поля:\nm = ");
            int m = int.Parse(Console.ReadLine());

            Console.WriteLine("Введите коэффициенты первого многочлена в порядке возрастания его степеней:");
            int[] p = Console.ReadLine()
                    .Split(new char[] { ' ' })
                    .Select(n => (int.Parse(n) % m + m) % m)
                    .ToArray();
            Polynomial p0 = new Polynomial(m, p);

            Console.WriteLine("Введите коэффициенты второго многочлена в порядке возрастания его степеней:");
            p = Console.ReadLine()
                    .Split(new char[] { ' ' })
                    .Select(n => (int.Parse(n) % m + m) % m)
                    .ToArray();
            Polynomial p1 = new Polynomial(m, p);

            Polynomial g0 = new Polynomial(m, 1);
            Polynomial g1 = new Polynomial(m, 0);
            Polynomial f0 = new Polynomial(m, 0);
            Polynomial f1 = new Polynomial(m, 1);

            Polynomial pr0 = new Polynomial(m, p0);
            Polynomial pr1 = new Polynomial(m, p1);

            while (!p1.isZero())
            {
                Polynomial r;
                Polynomial q = pdf(new Polynomial(m, p0), new Polynomial(m, p1), m, out r);

                Polynomial tmp = new Polynomial(m, p0);
                p0 = new Polynomial(m, p1);
                p1 = tmp - p1 * q;

                tmp = new Polynomial(m, g0);
                g0 = new Polynomial(m, g1);
                g1 = tmp - g1 * q;

                tmp = new Polynomial(m, f0);
                f0 = new Polynomial(m, f1);
                f1 = tmp - f1 * q;
            }

            Console.WriteLine();
            Console.WriteLine("НОД = " + p0.ToString());
            Console.WriteLine("g(x) = " + g0.ToString());
            Console.WriteLine("f(x) = " + f0.ToString());
        }
    }
}
