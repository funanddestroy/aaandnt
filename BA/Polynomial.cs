﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BA
{
    class Polynomial
    {
        private int[] coefficients;
        private static int m;

        public Polynomial(int m1, params int[] coefficients)
        {
            m = m1;
            this.coefficients = coefficients;
        }

        public Polynomial(int m1, Polynomial o)
        {
            m = m1;
            this.coefficients = (int[])o.coefficients.Clone();
        }

        public int this[int n]
        {
            get { return coefficients[n]; }
            set { coefficients[n] = value; }
        }
        
        public int deg()
        {
            int i = coefficients.Length - 1;           

            while (i > 0 && coefficients[i] == 0)
            {
                i--;
            }

            return i;
        }

        public override string ToString()
        {
            int deg = this.deg();
            string result = "";

            if (this.isZero())
            {
                return "" + 0;
            }

            int firstTerm = (coefficients[deg]);

            if (firstTerm < 0)
            {
                result += "-";
            }

            if (Math.Abs(firstTerm) != 1 || (Math.Abs(firstTerm) == 1 && deg == 0))
            {
                result += Math.Abs(firstTerm);
            }

            if (deg > 0)
            {
                result += "x";
            }

            if (deg > 1)
            {
                result += "^" + deg;
            }

            for (int i = deg - 1; i >= 0; i--)
            {
                if (coefficients[i] == 0)
                {
                    continue;
                }

                result += (coefficients[i] < 0) ? " - " : " + ";

                if (Math.Abs(coefficients[i]) != 1 || (Math.Abs(coefficients[i]) == 1 && i == 0))
                {
                    result += Math.Abs(coefficients[i]);
                }

                if (i >= 2)
                {
                    result += "x" + "^" + i;
                }
                else if (i == 1)
                {
                    result += "x";
                }
            }
            return result;
        }

        public bool isZero()
        {
            for (int i = 0; i < coefficients.Length; i++)
            {
                if (coefficients[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static Polynomial operator +(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = ((a + b) % m + m) % m;
            }
            return new Polynomial(m, result);
        }

        public static Polynomial operator -(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = ((a - b) % m + m) % m;
            }
            return new Polynomial(m, result);
        }

        public static Polynomial operator *(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = pFirst.coefficients.Length + pSecond.coefficients.Length - 1;
            var result = new int[itemsCount];
            for (int i = 0; i < pFirst.coefficients.Length; i++)
            {
                for (int j = 0; j < pSecond.coefficients.Length; j++)
                {
                    result[i + j] = (result[i + j] + pFirst[i] * pSecond[j]) % m;
                }
            }

            return new Polynomial(m, result);
        }
    }
}
