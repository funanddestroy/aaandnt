﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BA
{
    class Program
    {
        private static Polynomial pdf(Polynomial p1, Polynomial p2, int p, out Polynomial r)
        {
            int m = p1.deg();
            int n = p2.deg();

            if (m < n)
            {
                Polynomial tmp = p1;
                p1 = p2;
                p2 = tmp;
                int tp = n;
                n = m;
                m = tp;
            }

            Polynomial q = new Polynomial(p, new int[m - n + 1]);

            for (int k = m - n; k >= 0; k--)
            {
                q[k] = ((p1[n + k] * inverse(p2[n], p)) % p + p) % p;
                for (int j = n + k - 1; j >= k; j--)
                {
                    p1[j] = ((p1[j] - q[k] * p2[j - k]) % p + p) % p;
                }
            }

            r = new Polynomial(p, new int[] { 0 });

            if (n > 0)
            {
                r = new Polynomial(p, new int[n]);
                for (int i = 0; i < n; i++)
                {
                    r[i] = p1[i];
                }
            }

            return q;
        }

        private static Polynomial gcd(int m, Polynomial polynomialFirst, Polynomial polynomialSecond)
        {
            Polynomial p0 = new Polynomial(m, polynomialFirst);
            Polynomial p1 = new Polynomial(m, polynomialSecond);

            Polynomial g0 = new Polynomial(m, 1);
            Polynomial g1 = new Polynomial(m, 0);
            Polynomial f0 = new Polynomial(m, 0);
            Polynomial f1 = new Polynomial(m, 1);

            Polynomial pr0 = new Polynomial(m, p0);
            Polynomial pr1 = new Polynomial(m, p1);

            while (!p1.isZero())
            {
                Polynomial r;
                Polynomial q = pdf(new Polynomial(m, p0), new Polynomial(m, p1), m, out r);

                Polynomial tmp = new Polynomial(m, p0);
                p0 = new Polynomial(m, p1);
                p1 = tmp - p1 * q;

                tmp = new Polynomial(m, g0);
                g0 = new Polynomial(m, g1);
                g1 = tmp - g1 * q;

                tmp = new Polynomial(m, f0);
                f0 = new Polynomial(m, f1);
                f1 = tmp - f1 * q;
            }

            return p0;
        }

        private static int gcdex(int a, int b, int x, int y, out int outX, out int outY)
        {
            if (a == 0)
            {
                outX = 0; outY = 1;
                return b;
            }
            int x1 = 0;
            int y1 = 0;
            int d = gcdex(b % a, a, x1, y1, out x1, out y1);
            outX = y1 - (b / a) * x1;
            outY = x1;
            return d;
        }

        private static int inverse(int a, int m)
        {
            a = (a % m + m) % m;
            int x = 0;
            int y = 0;
            int g = gcdex(a, m, x, y, out x, out y);
            if (g != 1)
                return -1;
            else
            {
                x = (x % m + m) % m;
                return x;
            }
        }

        static private int[,] getQ(Polynomial p, int m)
        {
            int n = p.deg();
            int[,] Q = new int[n, n];

            int[] r = new int[n];
            r[0] = 1;
            for (int j = 0; j < m * 3 + 1; j++)
            {
                if (j % m == 0)
                {
                    int jm = j / m;
                    for (int i = 0; i < n; i++)
                    {
                        Q[jm, i] = r[i];
                    }
                }
                int t = r[n - 1];
                for (int i = n - 1; i > 0; i--)
                {
                    r[i] = ((r[i - 1] - t * p[i]) % m + m) % m;
                }
                r[0] = ((-t * p[0]) % m + m) % m;
            }
            return Q;
        }

        static private int[,] getQI(int[,] Q, int m)
        {
            int[,] QI = new int[Q.GetLength(0), Q.GetLength(1)];

            for (int i = 0; i < Q.GetLength(0); i++)
            {
                for (int j = 0; j < Q.GetLength(1); j++)
                {
                    QI[i, j] = Q[i, j];
                    if (i == j)
                    {
                        QI[i, j] = ((QI[i, j] - 1) % m + m) % m;
                    } 
                }
            }

            return QI;
        }

        static private int[,] NS(int[,] QI, int m, out int r)
        {
            int n = QI.GetLength(0);
            int[,] b = new int[n, n];

            r = 0;
            int[] c = new int[n];
            for (int i = 0; i < c.Length; i++)
            {
                c[i] = -1;
            }

            for (int h = 0; h < n; h++)
            {
                for (int j = 0; j < n; j++)
                {
                    if (QI[h, j] != 0 && c[j] < 0)
                    {
                        int q = ((-inverse(QI[h, j], m)) % m + m) % m;

                        for (int i = 0; i < n; i++)
                        {
                            QI[i, j] = ((QI[i, j] * q) % m + m) % m;
                        }

                        for (int i = 0; i < n; i++)
                        {
                            if (j != i)
                            {
                                int qi = QI[h, i];
                                for (int k = 1; k < n; k++)
                                {
                                    QI[k, i] = ((QI[k, i] + QI[k, j] * qi) % m + m) % m;
                                }
                            }
                        }
                        c[j] = h;
                    }
                }

                for (int j = 0; j < n; j++)
                {
                    if (j == h)
                    {
                        b[r, j] = 1;
                    }
                    else
                    {
                        for (int k = 0; k < n; k++)
                        {
                            if (c[k] == j && j > 0)
                            {
                                b[r, j] = QI[h, k];
                                break;
                            }
                        }
                    }
                }
                r++;
            }

            r--;
            return b;
        }

        static void Main(string[] args)
        {
            int m = 13;
            Polynomial p = new Polynomial(m, 8, 2, 1, 4, 1);
            int n = p.deg();
            int[,] Q = getQ(p, m);
            int[,] QI = getQI(Q, m);
            int r;
            int[,] ns = NS(QI, m, out r);
            int[] s = new int[n];
            for (int i = 0; i < n; i++)
            {
                s[i] = ns[r, i];
            }

            for (int i = 0; i < m; i++)
            {
                int[] s1 = (int[])s.Clone();
                s1[0] = s1[0] + m - i;
                Polynomial q = gcd(m, p, new Polynomial(m, s1));
                int deg = q.deg();
                int d = inverse(q[deg], m);

                if (deg > 0) {
                    for (int j = 0; j <= deg; j++) {
                        q[j] = (q[j] * d) % m;
                    }
                    Console.WriteLine(q);
                }
            }
        }
    }
}
