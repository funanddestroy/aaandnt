﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Kronecker__dimensional_
{
    class Program
    {
        static private List<int> getDivisors(int x)
        {
            List<int> divisorList = new List<int>();
            int maxDivisor = Math.Abs(x);

            for (int divisor = 1; divisor <= maxDivisor; divisor++)
            {
                if (maxDivisor % divisor == 0)
                {
                    divisorList.Add(divisor);
                    divisorList.Add(-divisor);
                }
            }
            return divisorList;
        }

        static private double[] plus (double[] pFirst, double[] pSecond)
        {
            int itemsCount = Math.Max(pFirst.Length, pSecond.Length);
            var result = new double[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                double a = 0;
                double b = 0;
                if (i < pFirst.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a + b;
            }
            return result;
        }

        static private Polynomial interp(int[] x, int[] y)
        {
            double[] res1 = new double[x.Length];
            for (int i = 0; i < y.Length; i++)
            {
                double k = 1;
                for (int j = 0; j < x.Length; j++)
                {
                    if (j != i)
                    {
                        k *= (x[i] - x[j]);
                    }
                }
                k = (double)y[i] / (double)k;

                Polynomial q = new Polynomial(1);
                for (int j = 0; j < x.Length; j++)
                {
                    if (j != i)
                    {
                        q *= new Polynomial(-x[j], 1);
                    }
                }

                double[] res2 = new double[q.deg() + 1];
                for (int h = 0; h < res2.Length; h++)
                {
                    res2[h] = q[h] * k;
                }
                res1 = plus(res1, res2);
            }

            int[] result = new int[res1.Length]; 
            for (int i = 0; i < res1.Length; i++)
            {
                result[i] = (int)Math.Round(res1[i], 0);
            }

            return new Polynomial(result);
        }

        static void Main(string[] args)
        {
            Polynomial f = new Polynomial(-1, 6, -8, -2, -1, 1);
            Polynomial g;
            int n = f.deg();

            for (int i = 0; i <= n / 2; i++)
            {
                if (f.сalculate(i) == 0)
                {
                    g = new Polynomial(-i, 1);
                    Console.WriteLine(g.ToString());
                    return;
                }
            }

            List<int> M = getDivisors(f.сalculate(0));
            List<List<int>> U = new List<List<int>>();
            List<List<int>> tmpU;
            for (int i = 0; i < M.Count; i++)
            {
                U.Add(new List<int>());
                U[i].Add(M[i]);
            }

            for (int i = 1; i <= n / 2; i++)
            {
                M = getDivisors(f.сalculate(i));

                tmpU = new List<List<int>>();
                for (int k = 0; k < U.Count; k++)
                {
                    tmpU.Add(new List<int>(U[k]));
                }

                U = new List<List<int>>();
                for (int k = 0; k < M.Count; k++)
                {
                    for (int h = 0; h < tmpU.Count; h++)
                    {
                        U.Add(new List<int>(tmpU[h]));
                        U[U.Count - 1].Add(M[k]);
                    }
                }
                foreach (List<int> u in U)
                {
                    int[] _i = new int[i + 1];
                    for (int k = 0; k < i + 1; k++)
                    {
                        _i[k] = k;
                    }
                    g = interp(_i, u.ToArray());
                    if ((f % g).isZero() && g.deg() > 0)
                    {
                        Console.WriteLine(g.ToString());
                        return;
                    }
                }
            }
        }
    }
}
