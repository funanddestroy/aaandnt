﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace СКВС
{
    class Polynomial
    {
        private int[] coefficients;
        private int cont = 1;

        private int gcd(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            else
            {
                return gcd(b, a % b);
            }
        }

        private int gcdm(int[] a)
        {
            switch (a.Length)
            {
                case 0:
                    return -1;
                case 1:
                    return a[0];
                case 2:
                    return gcd(a[0], a[1]);
                default:
                    int x = a[0];
                    for (int i = 1; i < a.Length; i++)
                    {
                        x = gcd(x, a[i]);
                    }
                    return x;
            }
        }

        private int calcCont()
        {
            int[] ret = this.getArray().Where(a => a != 0).ToArray();
            return gcdm(ret);
        }

        public Polynomial(params int[] coefficients)
        {
            this.coefficients = coefficients;
            this.cont = calcCont();
        }

        public Polynomial(Polynomial o)
        {
            this.coefficients = (int[])o.coefficients.Clone();
            this.cont = calcCont();
        }

        public int getCont()
        {
            return cont;
        }

        public Polynomial getPrimitivePart()
        {
            int[] ret = this.getArray();
            for (int i = 0; i < ret.Length; i++)
            {
                ret[i] /= cont;
            }

            int ord = this.deg();

            if (ret[ord] < 0) 
            {
                for (int i = 0; i < ret.Length; i++)
                {
                    ret[i] *= -1;
                }
            }

            return new Polynomial(ret);
        }

        public int[] getArray()
        {
            int[] ret = new int[coefficients.Length];
            Array.Copy(coefficients, ret, coefficients.Length);
            return ret;
        }

        public int this[int n]
        {
            get { return coefficients[n]; }
            set { coefficients[n] = value; }
        }
        
        public int deg()
        {
            int i = coefficients.Length - 1;           

            while (i > 0 && coefficients[i] == 0)
            {
                i--;
            }

            return i;
        }

        public void trim()
        {
            int ord = this.deg();
            int[] res = new int[ord + 1];
            for (int i = 0; i < ord + 1; i++)
            {
                res[i] = this.coefficients[i];
            }

            this.coefficients = res;
        }

        public override string ToString()
        {
            int deg = this.deg();
            string result = "";

            if (this.isZero())
            {
                return "" + 0;
            }

            int firstTerm = (coefficients[deg]);

            if (firstTerm < 0)
            {
                result += "-";
            }

            if (Math.Abs(firstTerm) != 1 || (Math.Abs(firstTerm) == 1 && deg == 0))
            {
                result += Math.Abs(firstTerm);
            }

            if (deg > 0)
            {
                result += "x";
            }

            if (deg > 1)
            {
                result += "^" + deg;
            }

            for (int i = deg - 1; i >= 0; i--)
            {
                if (coefficients[i] == 0)
                {
                    continue;
                }

                result += (coefficients[i] < 0) ? " - " : " + ";

                if (Math.Abs(coefficients[i]) != 1 || (Math.Abs(coefficients[i]) == 1 && i == 0))
                {
                    result += Math.Abs(coefficients[i]);
                }

                if (i >= 2)
                {
                    result += "x" + "^" + i;
                }
                else if (i == 1)
                {
                    result += "x";
                }
            }
            return result;
        }

        public bool isZero()
        {
            for (int i = 0; i < coefficients.Length; i++)
            {
                if (coefficients[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public static Polynomial operator +(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a + b;
            }
            return new Polynomial(result);
        }

        public static Polynomial operator -(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a - b;
            }
            return new Polynomial(result);
        }

        public static Polynomial operator *(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = pFirst.coefficients.Length + pSecond.coefficients.Length - 1;
            var result = new int[itemsCount];
            for (int i = 0; i < pFirst.coefficients.Length; i++)
            {
                for (int j = 0; j < pSecond.coefficients.Length; j++)
                {
                    result[i + j] += pFirst[i] * pSecond[j];
                }
            }

            return new Polynomial(result);
        }

        public static Polynomial operator /(Polynomial pFirst, Polynomial pSecond)
        {
            pFirst.trim();
            pSecond.trim();

            int[] dividend = pFirst.getArray();
            int[] divisor = pSecond.getArray();

            if (dividend.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делимого не может быть 0");
            }
            if (divisor.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делителя не может быть 0");
            }

            int[] remainder = (int[])dividend.Clone();
            int[] quotient = new int[remainder.Length - divisor.Length + 1];
            for (int i = 0; i < quotient.Length; i++)
            {
                int coeff = remainder[remainder.Length - i - 1] / divisor.Last();
                quotient[quotient.Length - i - 1] = coeff;
                for (int j = 0; j < divisor.Length; j++)
                {
                    remainder[remainder.Length - i - j - 1] -= coeff * divisor[divisor.Length - j - 1];
                }
            }

            return new Polynomial(quotient);
        }

        public static Polynomial operator %(Polynomial pFirst, Polynomial pSecond)
        {
            pFirst.trim();
            pSecond.trim();

            int[] dividend = pFirst.getArray();
            int[] divisor = pSecond.getArray();

            if (dividend.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делимого не может быть 0");
            }
            if (divisor.Last() == 0)
            {
                throw new ArithmeticException("Старший член многочлена делителя не может быть 0");
            }

            int[] remainder = (int[])dividend.Clone();
            int[] quotient = new int[remainder.Length - divisor.Length + 1];
            for (int i = 0; i < quotient.Length; i++)
            {
                int coeff = remainder[remainder.Length - i - 1] / divisor.Last();
                quotient[quotient.Length - i - 1] = coeff;
                for (int j = 0; j < divisor.Length; j++)
                {
                    remainder[remainder.Length - i - j - 1] -= coeff * divisor[divisor.Length - j - 1];
                }
            }

            return new Polynomial(remainder);
        }
    }
}
