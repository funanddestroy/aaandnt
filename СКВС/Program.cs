﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace СКВС
{
    class Program
    {
        private static int gcd(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            else
            {
                return gcd(b, a % b);
            }
        }

        private static Polynomial gcd(Polynomial polynomialFirst, Polynomial polynomialSecond)
        {
            Polynomial p1 = new Polynomial(polynomialFirst);
            Polynomial p2 = new Polynomial(polynomialSecond);

            int cp1 = p1.getCont();
            int cp2 = p2.getCont();

            int c = gcd(cp1, cp2);

            Polynomial p = p1.getPrimitivePart();
            Polynomial q = p2.getPrimitivePart();

            Polynomial pq;

            do
            {
                int m = p.deg();
                int n = q.deg();

                int lc = (int)Math.Pow((double)q[n], (double)(m - n + 1));

                for (int i = 0; i < m + 1; i++)
                {
                    p[i] *= lc;
                }

                pq = p % q;

                p = q.getPrimitivePart();
                q = pq.getPrimitivePart();

            } while (!pq.isZero());

            int ord = p.deg();
            if (ord == 0)
            { 
                return new Polynomial(c);
            }
            else
            {
                for (int i = 0; i < ord + 1; i++)
                {
                    p[i] *= c;
                }

                return p;
            }
        }

        private static Polynomial derivative(Polynomial polynomial)
        {
            int[] der = new int[polynomial.deg()];

            for (int i = der.Length - 1; i >= 0; i--)
            {
                der[i] = (i + 1) * polynomial[i + 1];
            }

            return new Polynomial(der);
        }

        static void Main(string[] args)
        {
            Polynomial p = new Polynomial(-1, 1, 2, -2, -1, 1);
            Polynomial r = gcd(p, derivative(p));
            Polynomial t = p / r;

            List<Polynomial> s = new List<Polynomial>(); 

            int j = 0;
            while (r.deg() != 0)
            {
                Polynomial v = gcd(r, t);
                s.Add(new Polynomial(t / v));
                r /= v;
                t = v;
                j++;
            }

            int e = j;
            s.Add(new Polynomial(t));

            for (int i = 0; i < s.Count; i++)
            {
                Console.WriteLine("S" + (i + 1) + " = " + s[i].ToString());
            }

            Console.WriteLine("e = " + e);
        }
    }
}
