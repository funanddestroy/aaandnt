﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horner
{
    class Program
    {
        static void Main(string[] args)
        {

            //Polynomial p = new Polynomial(45, 114, 71, -20, -21, 2, 1);

            Console.WriteLine("╔═════════════════════════════════════════════════════════════════════════════╗");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║                                Схема Горнера                                ║");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║            Программа для расчета целых корней многочлена с целыми           ║");
            Console.WriteLine("║               коэффициентами и для расчета значений многочлена              ║");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("╚═════════════════════════════════════════════════════════════════════════════╝");
            Console.WriteLine();

            Console.WriteLine("Введите коэффициенты многочлена в порядке возрастания его степеней:");
            int[] p0 = Console.ReadLine()
                    .Split(new char[] { ' ' })
                    .Select(n => int.Parse(n))
                    .ToArray();
            Polynomial p = new Polynomial(p0);

            int[] roots = p.getRoots();

            Console.Write("Корни многочлена: ");
            for (int i = 0; i < roots.Length; i++)
            {
                if (i == roots.Length - 1)
                {
                    Console.WriteLine(roots[i]);
                    break;
                }
                Console.Write(roots[i] + ", ");
            }

            while (true)
            {
                Console.WriteLine();
                Console.WriteLine("Введите значение для расчета:\n(при вводе не числа произойдет выход из цикла)");
                string line = Console.ReadLine();
                int x = 0;

                try
                {
                    x = int.Parse(line);   
                }
                catch
                {
                    break;
                }

                Console.WriteLine("f(" + x + ") = " + p.сalculate(x));
            }
        }
    }
}
