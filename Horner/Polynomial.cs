﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Horner
{
    class Polynomial
    {
        private int[] coefficients;

        public Polynomial(params int[] coefficients)
        {
            this.coefficients = coefficients;
        }

        public Polynomial(Polynomial o)
        {
            this.coefficients = (int[])o.coefficients.Clone();
        }

        public int this[int n]
        {
            get { return coefficients[n]; }
            set { coefficients[n] = value; }
        }

        public int order()
        {
            int i = coefficients.Length - 1;

            while (i > 0 && coefficients[i] == 0)
            {
                i--;
            }

            return i;
        }

        public override string ToString()
        {
            int ord = this.order();
            string ret = "";
            for (int i = ord; i > 1; i--)
            {
                if (coefficients[i] != 0)
                {
                    ret += coefficients[i] + "x^" + i;
                    if (i - 1 != 0 && i > 3)
                    {
                        ret += " + ";
                    }
                }
            }

            if (ord > 0 && coefficients[1] != 0 && ret.Equals(""))
            {
                ret += coefficients[1] + "x";
            }
            else if (ord > 0 && coefficients[1] != 0)
            {
                ret += " + " + coefficients[1] + "x";
            }

            if (ord >= 0 && coefficients[0] != 0 && ret.Equals(""))
            {
                ret += coefficients[0];
            }
            else if (ord >= 0 && coefficients[0] != 0)
            {
                ret += " + " + coefficients[0];
            }

            return ret;
        }

        public bool isZero()
        {
            for (int i = 0; i < coefficients.Length; i++)
            {
                if (coefficients[i] != 0)
                {
                    return false;
                }
            }

            return true;
        }

        public int сalculate(int x)
        {
            int n = coefficients.Length - 1;
            int result = 0;
            for (int i = n; i >= 0; i--)
            {
                result = x * result + coefficients[i];
            }
            return result;
        }

        public int[] getRoots()
        {
            List<int> rootList = new List<int>();
            int a = Math.Abs(coefficients[0]);
            int maxDivisor = a / 2;

            for (int divisor = 1; divisor < maxDivisor; divisor++)
            {
                if (a % divisor != 0)
                {
                    continue;
                }

                if (сalculate(divisor) == 0)
                {
                    rootList.Add(divisor);
                }
                if (сalculate(-divisor) == 0)
                {
                    rootList.Add(-divisor);
                }
            }

            if (сalculate(a) == 0)
            {
                rootList.Add(a);
            }
            if (сalculate(-a) == 0)
            {
                rootList.Add(-a);
            }

            return rootList.ToArray();
        }
                
        public static Polynomial operator +(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a + b;
            }
            return new Polynomial(result);
        }

        public static Polynomial operator -(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = Math.Max(pFirst.coefficients.Length, pSecond.coefficients.Length);
            var result = new int[itemsCount];
            for (int i = 0; i < itemsCount; i++)
            {
                int a = 0;
                int b = 0;
                if (i < pFirst.coefficients.Length)
                {
                    a = pFirst[i];
                }
                if (i < pSecond.coefficients.Length)
                {
                    b = pSecond[i];
                }
                result[i] = a - b;
            }
            return new Polynomial(result);
        }

        public static Polynomial operator *(Polynomial pFirst, Polynomial pSecond)
        {
            int itemsCount = pFirst.coefficients.Length + pSecond.coefficients.Length - 1;
            var result = new int[itemsCount];
            for (int i = 0; i < pFirst.coefficients.Length; i++)
            {
                for (int j = 0; j < pSecond.coefficients.Length; j++)
                {
                    result[i + j] += pFirst[i] * pSecond[j];
                }
            }

            return new Polynomial(result);
        }
    }
}
