﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GEA_P
{
    class Program
    {
        private static int gcd(int a, int b)
        {
            if (b == 0)
            {
                return a;
            }
            else
            {
                return gcd(b, a % b);
            }
        }

        static void Main(string[] args)
        {
            Console.WriteLine("╔═════════════════════════════════════════════════════════════════════════════╗");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║                                     GEA-P                                   ║");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("║            Программа для расчета НОД многочленов с использованием           ║");
            Console.WriteLine("║             обобщенного алгоритма Евклида над полем целых чисел             ║");
            Console.WriteLine("║                                                                             ║");
            Console.WriteLine("╚═════════════════════════════════════════════════════════════════════════════╝");
            Console.WriteLine();

            Console.WriteLine("Введите коэффициенты первого многочлена в порядке возрастания его степеней:");
            int[] p0 = Console.ReadLine()
                    .Split(new char[] { ' ' })
                    .Select(n => int.Parse(n))
                    .ToArray();
            Polynomial p1 = new Polynomial(p0);

            Console.WriteLine("Введите коэффициенты второго многочлена в порядке возрастания его степеней:");
            p0 = Console.ReadLine()
                    .Split(new char[] { ' ' })
                    .Select(n => int.Parse(n))
                    .ToArray();
            Polynomial p2 = new Polynomial(p0);

            int cp1 = p1.getCont();
            int cp2 = p2.getCont();

            int c = gcd(cp1, cp2);

            Polynomial p = p1.getPrimitivePart();
            Polynomial q = p2.getPrimitivePart();

            Polynomial pq;

            do
            {
                int m = p.order();
                int n = q.order();

                int lc = (int)Math.Pow((double)q[n], (double)(m - n + 1));

                for (int i = 0; i < m + 1; i++)
                {
                    p[i] *= lc;
                }

                pq = p % q;
                
                p = q.getPrimitivePart();
                q = pq.getPrimitivePart();

            } while (!pq.isZero());

            int ord = p.order();
            if (ord == 0)
            {
                Console.WriteLine("НОД = " + c);
            }
            else 
            {
                for (int i = 0; i < ord + 1; i++) 
                {
                    p[i] *= c;
                }
                Console.WriteLine("НОД = " + p.ToString());
            }
        }
    }
}
